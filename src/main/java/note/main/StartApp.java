package note.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import note.utils.ClasaException;

import note.model.Corigent;
import note.model.Medie;

import note.controller.NoteController;
import note.model.Nota;
import note.repository.ClasaRepositoryMock;
import note.repository.EleviRepositoryMock;
import note.repository.NoteRepositoryMock;
import note.utils.Constants;

//functionalitati
//F01.	 adaugarea unei note la o anumita materie (nr. matricol, materie, nota acordata);
//F02.	 calcularea mediilor semestriale pentru fiecare elev (nume, nr. matricol),
//F03.	 afisarea elevilor coringenti, ordonati descrescator dupa numarul de materii la care nu au promovat şi alfabetic dupa nume.


public class StartApp {

	/**
	 * @param args
	 * @throws ClasaException
	 */
	public static void main(String[] args) throws ClasaException {
		// TODO Auto-generated method stub
		NoteController ctrl = new NoteController(new NoteRepositoryMock(), new ClasaRepositoryMock(), new EleviRepositoryMock());
		List<Medie> medii = new LinkedList<Medie>();
		List<Corigent> corigenti = new ArrayList<Corigent>();

		if(args.length < 2) {
            System.out.println("Fisierele cu date nu au fost date");
            return;
        }

        try {
            ctrl.readElevi(args[0]);
            ctrl.readNote(args[1]);
        }
        catch (Exception e){
            System.out.println("Fisiere inexistente. Verificati si incercati din nou.");
            return;
        }

		ctrl.creeazaClasa(ctrl.getElevi(), ctrl.getNote());
		boolean gasit = false;
		while(!gasit) {
			System.out.println("1. Adaugare Nota");
			System.out.println("2. Calculeaza medii");
			System.out.println("3. Elevi corigenti");
			System.out.println("4. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in) );
		    try {
				int option = Integer.parseInt(br.readLine());
				switch(option) {
				case 1:
					System.out.println("Numar matricol elev:");
					int elev = Integer.parseInt(br.readLine());
					if(ctrl.getElev(elev) == null){
					    throw new ClasaException(Constants.elevInexistent);
                    }
					System.out.println("Materie:");
					String materie = br.readLine();
					System.out.println("Nota:");
					int nota = Integer.parseInt(br.readLine());
					Nota n = new Nota(elev, materie, nota);
					ctrl.addNota(n);
					break;
				case 2: medii = ctrl.calculeazaMedii();
						for(Medie medie:medii)
							System.out.println(medie);
						break;
				case 3: corigenti = ctrl.getCorigenti();
						for(Corigent corigent:corigenti)
							System.out.println(corigent);
						break;
				case 4: gasit = true;
						break;
				default: System.out.println("Introduceti o optiune valida!");
				}
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
			} catch (Exception e){
				System.out.println(e.getMessage());
			}
		}
	}

}
