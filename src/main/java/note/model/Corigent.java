package note.model;

public class Corigent {
	private int nrMaterii;
	private Elev elev;
	/**
	 * @return the numeElev
	 */
	
	public Corigent(Elev elev, int nrMaterii) {
		this.nrMaterii = nrMaterii;
		this.elev = elev;
	}

    public Elev getElev() {
        return elev;
    }

    public void setElev(Elev elev) {
        this.elev = elev;
    }

	/**
	 * @return the nrMaterii
	 */
	public int getNrMaterii() {
		return nrMaterii;
	}
	/**
	 * @param nrMaterii the nrMaterii to set
	 */
	public void setNrMaterii(int nrMaterii) {
		this.nrMaterii = nrMaterii;
	}

	public String toString() {
		return elev.getNume() + " -> " + nrMaterii;
	}
}
