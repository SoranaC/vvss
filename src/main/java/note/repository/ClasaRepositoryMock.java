package note.repository;

import java.util.*;

import note.utils.ClasaException;
import note.utils.Constants;

import note.model.Corigent;
import note.model.Elev;
import note.model.Medie;
import note.model.Nota;

public class ClasaRepositoryMock implements ClasaRepository{

	private HashMap<Elev, HashMap<String, List<Double>>> noteClasa;
	
	public ClasaRepositoryMock() {
		noteClasa = new HashMap<Elev, HashMap<String, List<Double>>>();
	}

	@Override
	public void creazaClasa(List<Elev> elevi, List<Nota> note) {
		List<String> materii = new LinkedList<String>();
		for(Nota nota : note) {
			if(!materii.contains(nota.getMaterie()))
					materii.add(nota.getMaterie());
		}
		for (Elev elev : elevi) {
			HashMap<String, List<Double>> situatie = new HashMap<String, List<Double>>();
			for(String materie : materii) {
				List<Double> noteMaterie = new LinkedList<Double>();
				for(Nota nota : note) 
					if(nota.getMaterie().equals(materie) && nota.getNrmatricol() == elev.getNrmatricol())
						noteMaterie.add(nota.getNota());
				situatie.put(materie, noteMaterie);
			}
			noteClasa.put(elev, situatie);
		}
		
	}

	@Override
	public HashMap<Elev, HashMap<String, List<Double>>> getNoteClasa() {
		// TODO Auto-generated method stub
		return noteClasa;
	}

	public Medie medieElev(Elev elev) throws ClasaException {
	    Medie m = new Medie(); // (1)
	    m.setElev(elev);
        HashMap note  = noteClasa.get(elev);

        if(note == null){ // <2>
            throw new ClasaException("Elevul nu exista!"); // (3)
        }
        else {
            int nrMaterii = 0; // (4)
            double sumaMedii = 0;
            double medieElev;
            for(String materie : noteClasa.get(elev).keySet()) { // <5>
                List<Double> noteElev = noteClasa.get(elev).get(materie); // (6)
                int nrNote = noteElev.size();
                int i = 0;
                double suma = 0;
                if(nrNote > 0) { //<7>
                    while(i < nrNote) { // <8>
                        double nota = noteElev.get(i); //(9)
                        suma += nota;
                        i++;
                    }
                    nrMaterii++; // (10)
                    sumaMedii = sumaMedii + suma/i;
                }
            }
            if(nrMaterii == 0){ // <11>
                throw new ClasaException("Elevul nu are note"); // (12)
            }

            medieElev = sumaMedii / nrMaterii; // (13)
            m.setMedie(medieElev);
        }

	    return m; // (14)
    } // (15)

	@Override
	public List<Medie> calculeazaMedii() throws ClasaException{
		List<Medie> medii = new LinkedList<Medie>();
		if(noteClasa.size() > 0) {
			for(Elev elev : noteClasa.keySet()) {
                try {
                    Medie medie = medieElev(elev);
                    medii.add(medie);
                }
                catch (ClasaException e){
                    System.out.println(elev.getNume() + " -> " + e.getMessage() );
                }
			}
		}
		else 
			throw new ClasaException(Constants.emptyRepository);
		return medii;
	}
	
	public void afiseazaClasa() {
		for(Elev elev : noteClasa.keySet()) {
			System.out.println(elev);
			for(String materie : noteClasa.get(elev).keySet()) {
				System.out.println(materie);
				for(double nota : noteClasa.get(elev).get(materie))
					System.out.print(nota + " ");
			}
		}
	}

	@Override
	public List<Corigent> getCorigenti() throws ClasaException {
		List<Corigent> corigenti = new ArrayList<Corigent>();
		if(noteClasa.size() > 0) {
			for(Elev elev : noteClasa.keySet()) {
				Corigent corigent = new Corigent(elev, 0);
				for(String materie : noteClasa.get(elev).keySet()) {
					List<Double> noteElev = noteClasa.get(elev).get(materie);
					int nrNote = noteElev.size();
					int i = 0;
					double suma = 0;
					if(nrNote > 0) {
						while(i < nrNote) {
							double nota = noteElev.get(i);
							suma += nota;
							i++;
						}
						double media = suma/i;
						if (media < 4.5)
							corigent.setNrMaterii(corigent.getNrMaterii() + 1);
					}
				}
				if(corigent.getNrMaterii() > 0) {
					int i = 0;
					while(i < corigenti.size() && corigenti.get(i).getNrMaterii() > corigent.getNrMaterii())
						i++;
					if(i != corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii()) {
						while(i < corigenti.size() && corigenti.get(i).getNrMaterii() == corigent.getNrMaterii() && corigenti.get(i).getElev().getNume().compareTo(corigent.getElev().getNume()) < 0)
							i++;
						corigenti.add(i, corigent);
					}
					else
						corigenti.add(i, corigent);
				}
			}
		}
		else{
		    throw new ClasaException("Nu exista note");
        }
		return corigenti;
	}

	@Override
	public void addNota(Nota nota, Elev elev){
	    //double e = nota.getNrmatricol();
	    String materie = nota.getMaterie();
	    double n = nota.getNota();

	    boolean existing = false;

	    if(noteClasa.size() > 0)
            for(String m : noteClasa.get(elev).keySet()){
                if(m.equals(materie)){
                    noteClasa.get(elev).get(m).add(n);
                    existing = true;
                }
            }

        if(!existing){
            noteClasa.get(elev).put(materie, new LinkedList<Double>(Arrays.asList(n)));
            for (Elev e : noteClasa.keySet()) {
                if(e.getNrmatricol()!=elev.getNrmatricol()){
                    noteClasa.get(e).put(materie, new LinkedList<Double>());
                }
            }
        }
    }
	
}
