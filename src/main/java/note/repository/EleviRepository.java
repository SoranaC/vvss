package note.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import note.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier) throws Exception;
	public Elev getElev(double nrMatricol);
}
